<?php

namespace Drupal\consultation;

use Drupal\Core\Extension\ModuleUninstallValidatorInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Validates module uninstall readiness based on existing consultation entities.
 */
class ConsultationUninstallValidator implements ModuleUninstallValidatorInterface {
  use StringTranslationTrait;

  /**
   * Constructs a new ConsultationUninstallValidator.
   */
  public function __construct() {
  }

  /**
   * {@inheritdoc}
   */
  public function validate($module) {
    $reasons = [];

    if ($module == 'consultation') {
      // Check if there are any consultation nodes.
      $query = \Drupal::entityQuery('node')
        ->condition('type', 'consultation')
        ->accessCheck(FALSE)
        ->range(0, 1);

      // If so, prevent the uninstall.
      if (!empty($query->execute())) {
        $reasons[] = $this->t('There is content for the consultation content type. You must remove consultation content before the module can be uninstalled.');
      }
    }

    return $reasons;
  }

}
